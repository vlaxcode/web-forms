<?php
    session_start();
    include('config.php');

    if (  isset($_SESSION['user_id'] )) {

      if ($_SESSION['user_id'] === 'auth') 
        header("Location: http://localhost/lab/web-forms/secret.php", true, 301);
}


    if (isset($_POST['login'])) {
        $username = $_POST['username'];
        $password = $_POST['password'];

        $result = false; 

        
        if ($user_credentials['user'] === $username) $result = true; 


        if (!$result) {
            echo '<p class="error">Username password combination is wrong!</p>';
        } else {
            if ($password === $user_credentials['password']) {
                $_SESSION['user_id'] = 'auth';
                //echo '<p class="success">Congratulations, you are logged in!</p>';
                header("Location: http://localhost/lab/web-forms/secret.php", true, 301);
            } else {
                echo '<p class="error">Username password combination is wrong!</p>';
            }
        }
    }
?>



<form method="post" action="" name="signin-form">
  <div class="form-element">
    <label>Username</label>
    <input type="text" name="username" pattern="[a-zA-Z0-9]+" required />
  </div>
  <div class="form-element">
    <label>Password</label>
    <input type="password" name="password" required />
  </div>
  <button type="submit" name="login" value="login">Log In</button>
</form>